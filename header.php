<!DOCTYPE html>
<html lang="ru" dir="ltr">
<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
  <link rel="stylesheet" href="/pub/css/index.css?ver=<?=mt_rand(0, 1000)?>"/>
  <link rel="shortcut icon" href="/favicon.ico">
  <title>Шахматная школа Ботвинника</title>
</head>

<body id="body" ontouchstart="">

  <header class="header"></header>

  <main>
