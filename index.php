<?
	include($_SERVER['DOCUMENT_ROOT'] . '/header.php');
?>

<div class="wrapper">
	<h1><span>Ботвинник</span> Михаил Моисеевич</h1>

	<div class="heroText">
		<p>Советский шахматист, 6-й в истории шахмат и 1-й советский чемпион мира. Гроссмейстер СССР, международный гроссмейстер и арбитр по шахматной композиции, заслуженный мастер спорта СССР, 6-кратный чемпион СССР, абсолютный чемпион СССР. Шестикратный победитель Шахматных Олимпиад в составе команды СССР (двукратный победитель в личном зачёте)</p>
	</div>

	<div class="quoteList">
		<input id="quote-karpov" type="checkbox" >
		<input id="quote-kramnik" type="checkbox" >

		<div class="quoteItem quoteItem--karpov">
			<div class="quoteItem__img">
				<img src="/uploads/img/karpov.png" alt="">
			</div>
			<div class="quoteItem__info">
				<div class="quoteItem__head">
					<div class="quoteItem__name">Анатолий Карпов</div>
					<div class="quoteItem__title">XII чемпион мира</div>
				</div>

				<div class="quoteItem__text">
					<p>Я познакомился с Михаилом Моисеевичем в 1962 году на сборах общества «Труд» в Подольске, где он читал лекцию. <span class="quoteItem__hidd">Но ближе узнал Ботвинника позднее, когда вместе еще с шестью юными шахматистами стал заниматься в его школе. Он был немногословен, говорил размеренно, вдумчиво, часто поправляя очки одним пальцем. Его уроки были скорее похожи на лекции.</span></p>
					<p class="quoteItem__hidd">Сам Ботвинник привнес в шахматы очень многое, особенно в плане организации: благодаря ему и президенту ФИДЕ шведу Фольке Роггарду возникла стройная система розыгрыша первенства мира</p>
				</div>
				<label class="quoteItem__switch" for="quote-karpov"></label>
			</div>
		</div>

		<div class="quoteItem quoteItem--kramnik">
			<div class="quoteItem__info">
				<div class="quoteItem__head">
					<div class="quoteItem__name">Владимир Крамник</div>
					<div class="quoteItem__title">XIV чемпион мира</div>
				</div>

				<div class="quoteItem__text">
					<p>С Ботвинником я впервые встретился на школе Ботвинника в 1987 году. <span class="quoteItem__hidd">Та поездка в Друскининкай для меня очень памятна. Я попал в школу благодаря начальнику почты в Туапсе, который по своей инициативе позвонил как-то Ботвиннику и рассказал обо мне. Михаил Моисеевич не отмахнулся и предложил прислать мои партии. Так я оказался в больших шахматах. Ботвинник помогал нам, как мог: например, «пробил» мне своим авторитетом пару важных юношеских турниров. Многие говорят о его сложном характере, но к нам он относился очень тепло, доброжелательно, отличался хорошим чувством юмора. В целом Ботвинник — безусловно, позитивная фигура, а главная его заслуга — становление советской шахматной школы, завоевание ею ведущих позиций в мире.</span></p>
				</div>
				<label class="quoteItem__switch" for="quote-kramnik"></label>
			</div>
			<div class="quoteItem__img">
				<img src="/uploads/img/kramnik.png" alt="">
			</div>
		</div>
	</div>

</div>

<?
	include($_SERVER['DOCUMENT_ROOT'] . '/footer.php');
?>
