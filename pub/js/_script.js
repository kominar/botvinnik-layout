jQuery(function($){
  $(document).ready(function() {

    /* Отправка всех форм */
    $('#anketa-form').submit(function(e) {
      e.preventDefault();
      var post_data = new FormData(this);
      var form = $(this).attr('id');
      $('#' + form + ' [type=submit]').prop('disabled', true);

      $.ajax({
          type: 'POST',
          url: '/anketa.php',
          dataType: 'json',
          contentType: false,
          processData: false,
          data: post_data,
          success: function(data){
              if (data['error']) {
                var output = '<div class="alert alert-danger">Произошла ошибка</div>';
              } else {
                var output = '<div class="alert alert-success">Спасибо, ваша заявка принята</div>';
                $('#' + form).trigger('reset');
              }
              $('#' + form + ' .formBLock__result').hide().html(output).slideDown();
              $('#' + form + ' [type=submit]').prop('disabled', false);
          },
          error: function (xhr, ajaxOptions, thrownError) {
              console.log(xhr.status);
              console.log(thrownError);
          }
      });
      return false;
    });

    if ($().inputmask) {
      $("[name=phone], input[type=tel]").inputmask("8 (999) 999-99-99");
      $('body').on("focus", "[name=phone], input[type=tel]", function() {
        $(this).inputmask("8 (999) 999-99-99");
      });
    }

    /* Открытые модального окна */
    $('.js-gotomodal').on('click', function() {
      var target = $(this).data('target');
      $('#' + target).fadeIn(400);
      $('.modal-overlay').show();
      $('body').addClass('modaled');
    });

    /* Закрытые модального окна */
    $('.modal__close').on('click', function() {
      $('.modal').fadeOut(400);
      $('.modal-overlay').hide();
      $('body').removeClass('modaled');
    });
    $('.modal-overlay').on('click', function() {
        $(this).hide();
        $('.modal').fadeOut(400);
        $('body').removeClass('modaled');
    });

  });
});
