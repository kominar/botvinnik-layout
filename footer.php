</main>

<footer id="footer" class="footer">
    <div class="wrapper">
        <h2>Контактные данные шахматной школы Ботвинника</h2>
        <div class="contactLinks">
            <a href="tel:+74991376919">8-499-137-69-19</a>
            <a href="tel:+74991378404">8-499-137-84-04</a>
            <a href="mailto:dvorecchess@mail.ru">dvorecchess@mail.ru</a>
            <div class="bttn js-gotomodal" data-target="modal-anketa" >Заполнить анкету</div>
        </div>
    </div>
</footer>

<div class="modal" id="modal-anketa">
  <div class="modal__close"></div>
	<div class="modal__content">
    <h2 class="modal__title">Анкета для набора в школу Ботвинника</h2>
		<form id="anketa-form" name="anketa-form" action="/" method="post">
				<div class="formBlock__item"><input type="text" name="surname" placeholder="Фамилия ребёнка:"></div>
				<div class="formBlock__item"><input type="text" name="name" placeholder="Имя ребёнка:"></div>
				<div class="formBlock__item"><input type="number" name="bday" placeholder="Год рождения:"></div>
				<div class="formBlock__item"><input type="number" name="fshr" placeholder="Рейтинг ФШР:"></div>
				<div class="formBlock__item"><input type="text" name="level" placeholder="Разряд:"></div>
				<div class="formBlock__item"><input type="text" name="phone" placeholder="Контактный телефон:" required=""></div>
				<div class="formBlock__item"><input type="text" name="email" placeholder="Контактный email:"></div>
				<div class="formBlock__item">
                    <textarea name="exp" cols="30" rows="5" placeholder="Занимался ранее (город, клуб, тренер)"></textarea>
                </div>
				<div class="formBlock__item"><button class="bttn bttn--cta bttn--full" type="submit" form="anketa-form">Отправить</button></div>
                <div class="formBLock__result"></div>
			</form>

	</div>
</div>

<div class="modal-overlay"></div>

<script src="/pub/js/jquery.js"></script>
<script src="/pub/js/jquery.inputmask.min.js"></script>
<script src="/pub/js/_script.js?ver=<?=mt_rand(0, 1000)?>"></script>

</body>
</html>

